#ifndef DEBUG_H
#define DEBUG_H

bool IsDebuggerAttached();

void err(const char* message, int line, const char* file);

#define assert(condition,message) if(!(condition)) { if(IsDebuggerAttached()){ _asm _emit 0xcc } else{ err((message),__LINE__,__FILE__); } }

#endif
