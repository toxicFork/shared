#include "../include/debug.h"
#include <iostream>

bool IsDebuggerAttached()
{
	unsigned long dw;
	__asm
	{
		push eax
			push ecx
			mov eax, fs:[0x18]
		mov eax, dword ptr [eax + 0x30]
		mov ecx, dword ptr [eax]
		mov dw, ecx
			pop ecx
			pop eax
	}
	return (dw & 0x00010000 ? true : false);
}


void err(const char* message, int line, const char* file){
	std::cerr<<"Error in \n File: \n  "<<file<<"\n Line "<<line<<":\n  "<<message<<std::endl;
	std::cin.ignore();
	exit(-5);
}