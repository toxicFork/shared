#ifndef DEBUG_H
#define DEBUG_H

#include "..\include\CLContext\CLContext.h"

void err(CLContext* self, const char* message, int line, const char* file);

#ifdef DEBUG
bool IsCTXDebuggerAttached();

#define assert(condition,message) if(!(condition)) { if(IsCTXDebuggerAttached()){ _asm _emit 0xcc } err(this,(message),__LINE__,__FILE__); }
#else
#define assert(condition,message) if(!(condition)) { err(this,(message),__LINE__,__FILE__); }
#endif

#endif
