#include "debug.h"
#include <iostream>
#include <sstream>

#ifdef DEBUG
bool IsCTXDebuggerAttached()
{
	unsigned long dw;
	__asm
	{
		push eax
			push ecx
			mov eax, fs:[0x18]
		mov eax, dword ptr [eax + 0x30]
		mov ecx, dword ptr [eax]
		mov dw, ecx
			pop ecx
			pop eax
	}
	return (dw & 0x00010000 ? true : false);
}
#endif

void err(CLContext* self, const char* message, int line, const char* file){
	self->isDead = true;
	if(!self->windowedMode){
		std::cerr<<"Error in \n File: \n  "<<file<<"\n Line "<<line<<":\n  "<<message<<std::endl;
		//std::cin.ignore();
		//exit(-5);
	}
	else{
		std::ostringstream o;
		o <<"Error in \n File: \n  "<<file<<"\n Line "<<line<<":\n  "<<message<<std::endl;
		MessageBox(NULL,o.str().c_str(),"Error!",NULL);
		//delete self;
	}
}