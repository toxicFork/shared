#pragma warning(push)
#pragma warning(disable:4290)
#include "..\include\CLContext\CLContext.h"
#pragma warning(pop)


#include <iostream>
#include <fstream>
#include <sstream>
#include <CL/cl_gl.h>

#include "debug.h"

void CLContext::CreateContext(int platformChoice){
	VECTOR_CLASS<Platform> platforms;
	Platform::get(&platforms);

	assert(platforms.size()>0,"There are no OpenCL platforms available!");
	assert(platforms.size()>(unsigned int)platformChoice,"Wrong platform!");

	platforms[platformChoice].getDevices(CL_DEVICE_TYPE_GPU,&devices);

	assert(devices.size()>0,"There are no OpenCL devices available!");

	if(DoublePrecisionEnabled){
		assert(checkDeviceForExtension(0,"cl_khr_fp64")||checkDeviceForExtension(0,"cl_amd_fp64"),"fp64  extension not available!");
	}
	if(CLGLEnabled){
		assert(checkDeviceForExtension(0,"cl_khr_gl_sharing"),"cl_khr_gl_sharing extension not available!");
		assert(wglGetCurrentContext(),"wglGetCurrentContext not available!");
		assert(wglGetCurrentDC(),"wglGetCurrentDC not available!");
		cl_context_properties cps[]=
		{
			CL_GL_CONTEXT_KHR, (cl_context_properties) wglGetCurrentContext(),
			CL_WGL_HDC_KHR, (cl_context_properties) wglGetCurrentDC(),
			CL_CONTEXT_PLATFORM, (cl_context_properties) (platforms[platformChoice])(),
			0
		};
		context = new Context( CL_DEVICE_TYPE_GPU, cps);
	} else{
		cl_context_properties cps[] = { 
			CL_CONTEXT_PLATFORM, 
			(cl_context_properties)(platforms[platformChoice])(), 
			0 
		};
		context = new Context( CL_DEVICE_TYPE_GPU, cps);
	}
	assert(context!=NULL,"context not found!");
}

void CLContext::CreateCommandQueue(Context *context, Device *device)
{
	commandQueue = new CommandQueue(*context, *device);
	assert(commandQueue,"failed to create commandqueue!");
}

program_type CLContext::CreateProgram( const char* fileName )
{
	std::ifstream sourceFile(fileName);
	assert(sourceFile.is_open(),"failed to open source file!");

	std::string sourceCode(
		std::istreambuf_iterator<char>(sourceFile),
		(std::istreambuf_iterator<char>()));
	Program::Sources source(1, std::make_pair(sourceCode.c_str(), sourceCode.length()));
	Program p(*context, source);
#ifdef DEBUG
	try {
#endif
		p.build(devices);
#ifdef DEBUG
	} catch (Error ex) {
		std::ostringstream o;
		o << "Error in program compilation of ("<< fileName << ") : " << std::endl;
		o << std::endl << "----------------------------------------" << std::endl << p.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) << std::endl << "----------------------------------------" << std::endl;
		assert(false,o.str().c_str());
		throw;
	}
#endif
	programs.push_back(p);

	STRING_CLASS buildInfo = p.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]);
	if(buildInfo.length()>0){
		std::ostringstream o;
		o << "Warnings in program compilation of ("<< fileName << ") : " << std::endl;
		o << std::endl << "----------------------------------------" << std::endl << p.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) << std::endl << "----------------------------------------" << std::endl;
		std::cout<<(o.str().c_str());
	}

	return (program_type)(programs.size()-1);
}

kernel_type CLContext::CreateKernel( program_type programID, const char* kernelName, bool activate /*= false*/ )
{
	assert(programs.size()>programID,"programs.size()<=programID");
	Kernel k(programs[programID], kernelName);
	kernels.push_back(k);
	kernel_type curKernel = (kernel_type)(kernels.size()-1);
	if(activate)
		SetActiveKernel(curKernel);
	return curKernel;
}

kernel_type CLContext::CreateProgramWithKernel( const char* fileName, const char* kernelName, bool activate/*=false*/ )
{
	return CreateKernel(CreateProgram(fileName),kernelName,activate);
}

mem_type CLContext::CreateBuffer(cl_mem_flags flags,::size_t size,void* host_ptr /* = NULL */, cl_int* err /* = NULL */){
	Buffer b(*context,flags,size,host_ptr,err);
	buffers.push_back(b);
	return (mem_type)(buffers.size()-1);
}

img_type CLContext::CreateImage2DGL(cl_mem_flags flags,	GLenum target,	GLint  miplevel, GLuint texobj,	cl_int * err){
		Image2DGL i(*context,flags,target,miplevel,texobj,err);
		images.push_back(i);
		return (img_type)(images.size()-1);
}

void CLContext::ListPlatforms(std::ostringstream *o){
	VECTOR_CLASS<Platform> platforms;
	Platform::get(&platforms);

	assert(platforms.size()>0,"there are no available platforms to list!");

	for (unsigned int i=0;i<platforms.size();i++)
	{
		Platform platform = platforms[i];
		std::string name, vendor, profile, version,extensions;
		platform.getInfo(CL_PLATFORM_NAME, &name);
		platform.getInfo(CL_PLATFORM_VENDOR, &vendor);
		platform.getInfo(CL_PLATFORM_VERSION, &version);
		platform.getInfo(CL_PLATFORM_EXTENSIONS, &extensions);

		*o << "Platform " << i << ":" << std::endl
			<<"\t\"" << name << "\" by \""<< vendor <<"\""<<std::endl
			<<"\t("<< version << ")"<<std::endl<<std::endl
			<<"\t with extensions ("<< extensions << ")"<<std::endl<<std::endl;


		VECTOR_CLASS<Device> devs;
		platform.getDevices(CL_DEVICE_TYPE_ALL,&devs);

		for (unsigned int j=0;j<devs.size();j++)
		{
			Device device = devs[j];
			device.getInfo(CL_DEVICE_NAME, &name);
			device.getInfo(CL_DEVICE_VENDOR, &vendor);
			device.getInfo(CL_DEVICE_VERSION, &version);
			device.getInfo(CL_DEVICE_EXTENSIONS, &extensions);

			*o << "\tDevice " << j << ":" << std::endl
				<<"\t\t\"" << name << "\" by \""<< vendor <<"\""<<std::endl
				<<"\t\t("<< version << ")"<<std::endl<<std::endl
				<<"\t\t with extensions ("<< extensions << ")"<<std::endl<<std::endl;

		}
	}
}

CLContext::~CLContext(){
	buffers.clear();
	images.clear();
	kernels.clear();
	programs.clear();
	devices.clear();
}

CLContext::CLContext(int argc, char** argv, int additionalOptions){
	isDead = false;
	DoublePrecisionEnabled = (additionalOptions&ENABLE_FP64)!=0;
	CLGLEnabled = (additionalOptions&ENABLE_CL_GL)!=0;
	windowedMode = (additionalOptions&WINDOWED_MODE)!=0;

	bool platformDefined = false;
	for(int i=1;i<argc;i+=2){
		if(strcmp(argv[i],"--platform")==0){
			if(argc<=i+1){
				std::ostringstream o;
				o << "Incorrect arguments"<<std::endl<<std::endl;
				assert(false,o.str().c_str());
			}
			platformChoice = atoi(argv[i+1]);
			platformDefined = true;
		}
	}

	if(!platformDefined){
		std::ostringstream o;
		o << "No platform selected!"<<std::endl<<std::endl;
		ListPlatforms(&o);
		o << "Picking the first platform"<<std::endl<<std::endl;
		printf("%s",o.str().c_str());
		platformChoice = 0;
		/*
		std::ostringstream o;
		o << "Incorrect arguments"<<std::endl<<std::endl;
		ListPlatforms(&o);
		assert(false,o.str().c_str());
		*/
	}
	Initialise();
}

void CLContext::Initialise(){
	CreateContext(platformChoice);
	CreateCommandQueue(context, &devices[0]);
}

bool CLContext::checkDeviceForExtension(unsigned int device, const char* extensionName){
	assert(devices.size()>device,"invalid device id!");
	char info[1024];

	devices[device].getInfo(CL_DEVICE_EXTENSIONS,&info);
	std::string stdDevString(info);

	unsigned int szOldPos = 0;
	unsigned int szSpacePos = stdDevString.find(' ',szOldPos);

	while(szSpacePos!=stdDevString.npos)
	{
		if(strcmp(extensionName,stdDevString.substr(szOldPos,szSpacePos-szOldPos).c_str())==0){
			return true;
		}
		do 
		{
			szOldPos = szSpacePos + 1;
			szSpacePos = stdDevString.find(' ',szOldPos);
		} while (szSpacePos==szOldPos);
	}
	return false;

}

void CLContext::SetActiveKernel( kernel_type toActivate )
{
	assert(kernels.size()>toActivate,"kernel size<=toActivate");
	activeKernel = toActivate;
}

void CLContext::SetArg( int arg, mem_type buf )
{
	kernels[activeKernel].setArg(arg,buffers[buf]);
}

void CLContext::SetArg( int arg, img_type img )
{
	kernels[activeKernel].setArg(arg,images[img]);
}

void CLContext::SetArg(cl_uint index, ::size_t size, const void* argPtr){
	assert(activeKernel!=-1,"there's no active kernel!");
	kernels[activeKernel].setArg(index,size,(void*)argPtr);
}

cl_int CLContext::ExecuteActiveKernelWithRange( int g, int l, const VECTOR_CLASS<Event>* events, Event* event)
{
	NDRange global(g);
	NDRange local(l);

	return commandQueue->enqueueNDRangeKernel(kernels[activeKernel],NullRange,global,local,events,event);
}


cl_int CLContext::ExecuteActiveKernelWithRange( NDRange global, NDRange local, const VECTOR_CLASS<Event>* events, Event* event){
	return commandQueue->enqueueNDRangeKernel(kernels[activeKernel],NullRange,global,local,events,event);
}

void CLContext::ReadBuffer( mem_type buf, cl_bool blocking,
	::size_t offset,
	::size_t size,
	void* ptr,
	const VECTOR_CLASS<Event>* events,
	Event* event )
{
	assert(buffers.size()>buf,"buffer size <= buf !");
	commandQueue->enqueueReadBuffer(buffers[buf],blocking,offset,size,ptr,events,event);
	//commandQueue->enqueueWriteBuffer(mem_type buf,cl_bool blocking, ::size_t offset, ::size_t size, const void*ptr, const VECTOR_CLASS<Event>* events = NULL,Event* event = NULL)
}

void CLContext::WriteBuffer( mem_type buf, cl_bool blocking,
	::size_t offset,
	::size_t size,
	const void*ptr,
	const VECTOR_CLASS<Event>* events /* = NULL */,
	Event* event /* = NULL */ )
{
	assert(buffers.size()>buf,"buffer size <= buf !");
	commandQueue->enqueueWriteBuffer(buffers[buf],blocking,offset,size,ptr,events,event);
}
void CLContext::WaitFor( Event *evt )
{
	VECTOR_CLASS<Event> e;
	e.push_back(*evt);
	commandQueue->enqueueWaitForEvents( e );
}

void *CLContext::MapBuffer(
	mem_type buf,
	cl_bool blocking,
	cl_map_flags flags,
	::size_t offset,
	::size_t size,
	const VECTOR_CLASS<Event>* events,
	Event* event,
	cl_int* e)
{
	assert(buffers.size()>buf,"buffers size <= buf !");
	return commandQueue->enqueueMapBuffer(buffers[buf],blocking,flags,offset,size,events,event,e);
}

void CLContext::UnMapBuffer( 
	mem_type buf,
	void* mapped_ptr,
	const VECTOR_CLASS<Event>* events ,
	Event* event )
{
	assert(buffers.size()>buf,"buffers size <= buf !");
	commandQueue->enqueueUnmapMemObject(buffers[buf],mapped_ptr,events,event);
}

void CLContext::FinishQueue()
{
	commandQueue->finish();
}

cl_int CLContext::getTextureInfo( img_type img,
	cl_gl_texture_info   paramName,
	std::size_t               paramValueSize,
	void *               paramValue,
	std::size_t *             paramValueSizeRet )
{
	assert(images.size()>img,"images size <= img !");
	return clGetGLTextureInfo(images[img](),paramName,paramValueSize,paramValue,paramValueSizeRet);
}

cl_int CLContext::AcquireTextureObjects( const VECTOR_CLASS<img_type>* mem_objects,
	const VECTOR_CLASS<Event>* events,
	Event* event )
{
	VECTOR_CLASS<Memory> v;
	if(mem_objects)
		for (unsigned int i=0;i<mem_objects->size();i++)
		{
			img_type img = (*mem_objects)[i];
			assert(images.size()>img,"images size <= img !");
			v.push_back(images[img]);
		}
	return commandQueue->enqueueAcquireGLObjects(&v,events,event);
}

cl_int CLContext::ReleaseTextureObjects( const VECTOR_CLASS<img_type>* mem_objects,
	const VECTOR_CLASS<Event>* events,
	Event* event )
{
	VECTOR_CLASS<Memory> v;

	if(mem_objects)
		for (unsigned int i=0;i<mem_objects->size();i++)
		{
			v.push_back(images[(*mem_objects)[i]]);
		}
	return commandQueue->enqueueReleaseGLObjects(&v,events,event);
}

void CLContext::ReleaseBuffer( img_type *img )
{
	assert(images.size()>*img,"images size <= img !");
	images.erase(images.begin()+*img);
	*img = -1;
}

void CLContext::RemoveKernel( kernel_type *k )
{
	assert(kernels.size()>*k,"images size <= img !");
	kernels.erase(kernels.begin()+*k);
	*k = -1;
}

cl_ulong CLContext::getMaxBufferSize()
{
	return devices[0].getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>();
}

char* CLContext::ErrorMessage( cl_int error )
{
	switch(error){
	case CL_SUCCESS:
		return "CL_SUCCESS";
	case CL_DEVICE_NOT_FOUND:
		return "CL_DEVICE_NOT_FOUND";
	case CL_DEVICE_NOT_AVAILABLE:
		return "CL_DEVICE_NOT_AVAILABLE";
	case CL_COMPILER_NOT_AVAILABLE:
		return "CL_COMPILER_NOT_AVAILABLE";
	case CL_MEM_OBJECT_ALLOCATION_FAILURE:
		return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
	case CL_OUT_OF_RESOURCES:
		return "CL_OUT_OF_RESOURCES";
	case CL_OUT_OF_HOST_MEMORY:
		return "CL_OUT_OF_HOST_MEMORY";
	case CL_PROFILING_INFO_NOT_AVAILABLE:
		return "CL_PROFILING_INFO_NOT_AVAILABLE";
	case CL_MEM_COPY_OVERLAP:
		return "CL_MEM_COPY_OVERLAP";
	case CL_IMAGE_FORMAT_MISMATCH:
		return "CL_IMAGE_FORMAT_MISMATCH";
	case CL_IMAGE_FORMAT_NOT_SUPPORTED:
		return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
	case CL_BUILD_PROGRAM_FAILURE:
		return "CL_BUILD_PROGRAM_FAILURE";
	case CL_MAP_FAILURE:
		return "CL_MAP_FAILURE";
	case CL_MISALIGNED_SUB_BUFFER_OFFSET:
		return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
	case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
		return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
#ifdef CL_VERSION_1_2
	case CL_COMPILE_PROGRAM_FAILURE:
		return "CL_COMPILE_PROGRAM_FAILURE";
	case CL_LINKER_NOT_AVAILABLE:
		return "CL_LINKER_NOT_AVAILABLE";
	case CL_LINK_PROGRAM_FAILURE:
		return "CL_LINK_PROGRAM_FAILURE";
	case CL_DEVICE_PARTITION_FAILED:
		return "CL_DEVICE_PARTITION_FAILED";
	case CL_KERNEL_ARG_INFO_NOT_AVAILABLE:
		return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
#endif
	case CL_INVALID_VALUE:
		return "CL_INVALID_VALUE";
	case CL_INVALID_DEVICE_TYPE:
		return "CL_INVALID_DEVICE_TYPE";
	case CL_INVALID_PLATFORM:
		return "CL_INVALID_PLATFORM";
	case CL_INVALID_DEVICE:
		return "CL_INVALID_DEVICE";
	case CL_INVALID_CONTEXT:
		return "CL_INVALID_CONTEXT";
	case CL_INVALID_QUEUE_PROPERTIES:
		return "CL_INVALID_QUEUE_PROPERTIES";
	case CL_INVALID_COMMAND_QUEUE:
		return "CL_INVALID_COMMAND_QUEUE";
	case CL_INVALID_HOST_PTR:
		return "CL_INVALID_HOST_PTR";
	case CL_INVALID_MEM_OBJECT:
		return "CL_INVALID_MEM_OBJECT";
	case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
		return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
	case CL_INVALID_IMAGE_SIZE:
		return "CL_INVALID_IMAGE_SIZE";
	case CL_INVALID_SAMPLER:
		return "CL_INVALID_SAMPLER";
	case CL_INVALID_BINARY:
		return "CL_INVALID_BINARY";
	case CL_INVALID_BUILD_OPTIONS:
		return "CL_INVALID_BUILD_OPTIONS";
	case CL_INVALID_PROGRAM:
		return "CL_INVALID_PROGRAM";
	case CL_INVALID_PROGRAM_EXECUTABLE:
		return "CL_INVALID_PROGRAM_EXECUTABLE";
	case CL_INVALID_KERNEL_NAME:
		return "CL_INVALID_KERNEL_NAME";
	case CL_INVALID_KERNEL_DEFINITION:
		return "CL_INVALID_KERNEL_DEFINITION";
	case CL_INVALID_KERNEL:
		return "CL_INVALID_KERNEL";
	case CL_INVALID_ARG_INDEX:
		return "CL_INVALID_ARG_INDEX";
	case CL_INVALID_ARG_VALUE:
		return "CL_INVALID_ARG_VALUE";
	case CL_INVALID_ARG_SIZE:
		return "CL_INVALID_ARG_SIZE";
	case CL_INVALID_KERNEL_ARGS:
		return "CL_INVALID_KERNEL_ARGS";
	case CL_INVALID_WORK_DIMENSION:
		return "CL_INVALID_WORK_DIMENSION";
	case CL_INVALID_WORK_GROUP_SIZE:
		return "CL_INVALID_WORK_GROUP_SIZE";
	case CL_INVALID_WORK_ITEM_SIZE:
		return "CL_INVALID_WORK_ITEM_SIZE";
	case CL_INVALID_GLOBAL_OFFSET:
		return "CL_INVALID_GLOBAL_OFFSET";
	case CL_INVALID_EVENT_WAIT_LIST:
		return "CL_INVALID_EVENT_WAIT_LIST";
	case CL_INVALID_EVENT:
		return "CL_INVALID_EVENT";
	case CL_INVALID_OPERATION:
		return "CL_INVALID_OPERATION";
	case CL_INVALID_GL_OBJECT:
		return "CL_INVALID_GL_OBJECT";
	case CL_INVALID_BUFFER_SIZE:
		return "CL_INVALID_BUFFER_SIZE";
	case CL_INVALID_MIP_LEVEL:
		return "CL_INVALID_MIP_LEVEL";
	case CL_INVALID_GLOBAL_WORK_SIZE:
		return "CL_INVALID_GLOBAL_WORK_SIZE";
	case CL_INVALID_PROPERTY:
		return "CL_INVALID_PROPERTY";
#ifdef CL_VERSION_1_2
	case CL_INVALID_IMAGE_DESCRIPTOR:
		return "CL_INVALID_IMAGE_DESCRIPTOR";
	case CL_INVALID_COMPILER_OPTIONS:
		return "CL_INVALID_COMPILER_OPTIONS";
	case CL_INVALID_LINKER_OPTIONS:
		return "CL_INVALID_LINKER_OPTIONS";
	case CL_INVALID_DEVICE_PARTITION_COUNT:
		return "CL_INVALID_DEVICE_PARTITION_COUNT";
#endif
	default:
		return "UNKNOWN_ERROR";
	}
}
