#ifndef CLCONTEXT_H
#define CLCONTEXT_H

#ifdef DEBUG
#define __CL_ENABLE_EXCEPTIONS
#endif
//#define __NO_STD_VECTOR 

#pragma warning(push)
#pragma warning(disable:4290)
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <CL/cl.hpp>
#pragma warning(pop)

#include <boost/serialization/strong_typedef.hpp>

using namespace cl;

BOOST_STRONG_TYPEDEF(unsigned int, mem_type);
BOOST_STRONG_TYPEDEF(unsigned int, img_type);
BOOST_STRONG_TYPEDEF(unsigned int, program_type);
BOOST_STRONG_TYPEDEF(unsigned int, kernel_type);

#define ENABLE_CL_GL	(1<<0)
#define WINDOWED_MODE	(1<<1)
#define ENABLE_FP64		(1<<2)

class CLContext{
private:
	Context *context;
	CommandQueue *commandQueue;
	VECTOR_CLASS<Device> devices;
	VECTOR_CLASS<Buffer> buffers;
	VECTOR_CLASS<Image2DGL> images;
	VECTOR_CLASS<Program> programs;
	VECTOR_CLASS<Kernel> kernels;
	int activeKernel;
	bool CLGLEnabled;
	bool DoublePrecisionEnabled;
	unsigned int platformChoice;
	void CreateContext(int platformChoice);
	void CreateCommandQueue(Context *context, Device *device);
	bool checkDeviceForExtension(unsigned int device, const char* extensionName);
	void ListPlatforms(std::ostringstream *o);
	void Initialise();
public:
	bool windowedMode;
	bool isDead;
	CLContext(int argc, char **argv, int additionalOptions = 0);
	~CLContext();
	program_type CreateProgram(const char* fileName);
	kernel_type CreateKernel(program_type programID, const char* kernelName, bool activate = false);
	kernel_type CreateProgramWithKernel(const char* fileName, const char* kernelName, bool activate=false);

	mem_type CreateBuffer(cl_mem_flags flags,::size_t size,void* host_ptr = NULL, cl_int* err = NULL);
	img_type CreateImage2DGL(cl_mem_flags flags, GLenum target, GLint miplevel, GLuint texobj, cl_int * err = NULL);

	void SetActiveKernel( kernel_type toActivate );
	void SetArg( int arg, mem_type buf );
	void SetArg( int arg, img_type img );
	void SetArg(cl_uint index, ::size_t size, const void* argPtr);
	cl_int ExecuteActiveKernelWithRange( int global, int local = 1, const VECTOR_CLASS<Event>* events = NULL, Event* event = NULL);
	cl_int ExecuteActiveKernelWithRange( NDRange global, NDRange local, const VECTOR_CLASS<Event>* events = NULL, Event* event = NULL);
	void ReadBuffer( mem_type buf, cl_bool blocking,
		::size_t offset,
		::size_t size,
		void* ptr,
		const VECTOR_CLASS<Event>* events = NULL,
		Event* event = NULL );
	void WriteBuffer( mem_type buf, cl_bool blocking,
		::size_t offset,
		::size_t size,
		const void*ptr,
		const VECTOR_CLASS<Event>* events = NULL,
		Event* event = NULL );
	void WaitFor( Event *evt );
	void *MapBuffer(
		mem_type buf,
		cl_bool blocking,
		cl_map_flags flags,
		::size_t offset,
		::size_t size,
		const VECTOR_CLASS<Event>* events = NULL,
		Event* event = NULL,
		cl_int* err = NULL);
	void UnMapBuffer( 
		mem_type buf,
		void* mapped_ptr,
		const VECTOR_CLASS<Event>* events = NULL,
		Event* event = NULL );
	void FinishQueue();
	cl_int getTextureInfo( img_type img,
		cl_gl_texture_info   paramName,
		std::size_t               paramValueSize,
		void *               paramValue,
		std::size_t *             paramValueSizeRet);
	cl_int AcquireTextureObjects( const VECTOR_CLASS<img_type>* mem_objects = NULL,
		const VECTOR_CLASS<Event>* events = NULL,
		Event* event = NULL );
	cl_int ReleaseTextureObjects( const VECTOR_CLASS<img_type>* mem_objects = NULL,
		const VECTOR_CLASS<Event>* events = NULL,
		Event* event = NULL );
	void ReleaseBuffer( img_type *textureBuffer );
	void RemoveKernel( kernel_type *k );

	cl_ulong getMaxBufferSize();
	static char* ErrorMessage( cl_int error );
};


#endif